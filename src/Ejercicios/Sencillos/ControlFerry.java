package Ejercicios.Sencillos;


import java.util.Scanner;

/**
 @description: Program representing the control of a ferry, introducing ten differents vehicles introduced by keyboard.
 @author: Adrian Antanyon
 @version: 18/05/2020
 */
public class ControlFerry {
    Scanner lector = new Scanner(System.in);
    public static void main(String[] args) {
        ControlFerry programa = new ControlFerry();

        programa.inici();

    }

    public void inici(){
        System.out.println("B I E N V E N I D O   A L   C O N T R O L   D E   F E R R I S");
        Vehiculos [] listaVehiculos = new Vehiculos[10];

        String [] preguntasCaracteristicasVehiculo = {"¿Qué tipo de vehículo es? (coche, moto, barco...): ","¿Por dónde desplaza? (aire, tierra, agua): ",
                "¿De qué color es?: ", "Nº de ocupantes posible: ", "Cantidad de ruedas: ",
                "Peso: ", "Largo: ", "Ancho: " };

        System.out.println("Contesta las preguntas para darnos información sobre los vehículos que quieres guardar, por favor");
        for (int i=0; i<listaVehiculos.length;i++){
            System.out.println("Vehículo " + (i+1));

            System.out.println(preguntasCaracteristicasVehiculo[0]);
            String tipo_vehiculo = lector.next();
            lector.nextLine();

            System.out.println(preguntasCaracteristicasVehiculo[1]);
            String medio_transporte = lector.next();
            lector.nextLine();

            System.out.println(preguntasCaracteristicasVehiculo[2]);
            String color = lector.next();
            lector.nextLine();

            System.out.println(preguntasCaracteristicasVehiculo[3]);
            comprobacionInt();
            int ocupantes = lector.nextInt();
            ocupantes = valorNegativoInt(ocupantes);

            System.out.println(preguntasCaracteristicasVehiculo[4]);
            comprobacionInt();
            int ruedas = lector.nextInt();
            ruedas = valorNegativoInt(ruedas);

            System.out.println(preguntasCaracteristicasVehiculo[5]);
            comprobacionDouble();
            double peso = lector.nextDouble();
            peso = valorNegativoDouble(peso);

            System.out.println(preguntasCaracteristicasVehiculo[6]);
            comprobacionDouble();
            double largo = lector.nextDouble();
            largo = valorNegativoDouble(largo);

            System.out.println(preguntasCaracteristicasVehiculo[7]);
            comprobacionDouble();
            double ancho = lector.nextDouble();
            ancho = valorNegativoDouble(ancho);

            listaVehiculos[i] = new Vehiculos(tipo_vehiculo, medio_transporte, color, ocupantes, ruedas, peso, largo, ancho);
            System.out.println();
        }

        System.out.println("\nLos vehículos introducidos son los siguientes:\n");
        for (int i=0; i<listaVehiculos.length;i++){
            System.out.println(listaVehiculos[i].getInfoVehiculo()+"\n");
        }
    }

    public void comprobacionInt(){

        while (!lector.hasNextInt()){
            System.out.println("El dato introducido debe ser un número entero, vuelva a introducirlo, por favor");
            lector.next();
        }
    }

    public int valorNegativoInt(int valor){
        while (valor<1){
            System.out.println("El número introducido no puede ser negativo o 0, vuelva a introducirlo, por favor");
            comprobacionInt();
            valor = lector.nextInt();
        }
        return valor;
    }

    public double valorNegativoDouble(double valor){
        while (valor<=0){
            System.out.println("El número introducido no puede ser negativo o 0, vuelva a introducirlo, por favor");
            comprobacionDouble();
            valor = lector.nextDouble();
        }
        return valor;
    }

    public void comprobacionDouble(){

        while (!lector.hasNextDouble()){
            System.out.println("El dato introducido debe ser un número decimal, vuelva a introducirlo, por favor");
            lector.next();
        }
    }


}

class Vehiculos {
    private String color, tipo_vehiculo, medio_transporte;
    private int ocupantes, ruedas;
    private double peso, largo, ancho;

    public Vehiculos(String tipo_vehiculo, String medio_transporte, String color, int ocupantes, int ruedas, double peso, double largo, double ancho) {
        this.tipo_vehiculo = tipo_vehiculo;
        this.medio_transporte = medio_transporte;
        this.ruedas = ruedas;
        this.largo = largo;
        this.ancho = ancho;
        this.peso = peso;
        this.ocupantes = ocupantes;
        this.color = color;
    }

    public String getInfoVehiculo() {

        return "Tipo de vehículo: " + tipo_vehiculo + " y se desplaza por: " + medio_transporte + "\n" +
                "Tiene " + ruedas + " ruedas, mide " + largo + " metros de largo y " + ancho + " de ancho, además pesa " + peso + " kg.\n" +
                "Por último, puede transportar un total de " + ocupantes + " personas y es de color " + color;
    }

}