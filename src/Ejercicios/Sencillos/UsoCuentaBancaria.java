package Ejercicios.Sencillos;

import java.util.Random;

/**
 @description: Creates a bank account and the necessary methods for its operation like account name, account number or balance.
 @author: Adrian Antanyon
 @version: 17/05/2020
 */
public class UsoCuentaBancaria{
    public static void main(String[] args) {

        CuentaBancaria adri = new CuentaBancaria("Adrián Antañón Orozco",14810.75);

        System.out.println("B I E N V E N I D O  A   H E L L ' S  B A N K");
        System.out.println("- - - - - - - - - - - - - - - - - - - - - - -" );

        System.out.println(adri.getSaldo());
        System.out.println(adri.getDatosCuentaBancaria());
        adri.setIngreso(1300.54);
        adri.setReintegro(869);

    }
}

class CuentaBancaria {

    private double saldo;
    private long numeroCuenta;
    private String nombreTitular;

    public CuentaBancaria(String nombreTitular, double saldo){
        this.saldo = saldo;
        this.nombreTitular = nombreTitular;

        Random generarNumCuenta = new Random();
        numeroCuenta = Math.abs(generarNumCuenta.nextLong());
    }

    public String getSaldo(){
        return "El saldo disponible en la cuenta bancaria es de " + saldo+" €";
    }

    public String getDatosCuentaBancaria(){
        return "Titular: " + nombreTitular + "\n" +
                "Número de cuenta: " + numeroCuenta+"\n" +
                "Saldo disponible: " + saldo+" €";
    }

    public void setIngreso(double ingreso){

        if (ingreso < 0){
            System.out.println("El ingreso no puede ser negativo, por lo que " + ingreso + " no es aceptado.");
        }else {
            saldo += ingreso;
            System.out.println("Ingreso efectuado de "+ingreso +" €, el saldo actual es de " + saldo + " €");
        }

    }

    public void setReintegro(double reintegro){
        saldo -= reintegro;
        System.out.println("Se han retirado "+reintegro+" €, el saldo de la cuenta ahora es de " + saldo+" €");
    }

}


