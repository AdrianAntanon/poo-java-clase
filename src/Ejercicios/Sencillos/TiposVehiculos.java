package Ejercicios.Sencillos;
/**
 @description: Proves with the heredity from class Vehiculos into like Coches or Barcos.
 @author: Adrian Antanyon
 @version: 22/05/2020
 */
public class TiposVehiculos {

    public static void main(String[] args) {
        Coches seat = new Coches("Coche","tierra","negro",5,4,1200,3.2,2,false);

        System.out.println(seat.getInfoVehiculo());

        System.out.println(seat.getNumeroPuertasCoche());

        seat.setCincoPuertas(true);

        System.out.println(seat.getNumeroPuertasCoche());

        Barcos canoa = new Barcos("Barco pesquero", "agua", "azul", 15,0,6500,15,4,false);

        System.out.println(canoa.getInfoVehiculo());

        System.out.println(canoa.getCrucero());

        canoa.setCrucero(true);

        System.out.println(canoa.getCrucero());

    }


}

class Coches extends Vehiculos{

    private boolean cincoPuertas;

    public Coches(String tipo_vehiculo, String medio_transporte, String color, int ocupantes, int ruedas, double peso, double largo, double ancho, boolean cincoPuertas) {
        super(tipo_vehiculo, medio_transporte, color, ocupantes, ruedas, peso, largo, ancho);
        this.cincoPuertas = cincoPuertas;
    }

    public void setCincoPuertas(boolean cincoPuertas_o_tres){
        cincoPuertas = cincoPuertas_o_tres;
    }

    public String getNumeroPuertasCoche(){

        if (cincoPuertas){
            return "El coche dispone de 5 puertas";
        }else {
            return "El coche dispone de 3 puertas";
        }
    }
}

class Barcos extends Vehiculos{
    private boolean crucero;

    public Barcos(String tipo_vehiculo, String medio_transporte, String color, int ocupantes, int ruedas, double peso, double largo, double ancho,boolean crucero) {
        super(tipo_vehiculo, medio_transporte, color, ocupantes, ruedas, peso, largo, ancho);
        this.crucero = crucero;
    }

    public void setCrucero(boolean crucero_o_no){
        crucero = crucero_o_no;
    }

    public String getCrucero(){
        if (crucero){
            return "El barco es un crucero enorme";
        }else {
            return "Más que un barco, esto es una canoa.";
        }
    }

}


