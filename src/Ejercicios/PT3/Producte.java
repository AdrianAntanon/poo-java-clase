package Ejercicios.PT3;
/**
 @description: The creation of a class Producte, his development and it's starts up.
 @author: Adrian Antanyon
 @version: 01/06/2020
 */
class Producte {
    private int codigo, stock=10;
    private String nombre, tipo;
    private double precio;

    public Producte(int codigo, String nombre, String tipo, double precio){
        this.codigo=codigo;
        this.nombre=nombre;
        this.tipo=tipo;
        this.precio=precio;
    }

    public int getCodigo(){
        return codigo;
    }
    public int getStock(){
        return stock;
    }
    public void setStock(int cantidad){stock = cantidad;}
    public void setPrecio(double precio){this.precio=precio;}
    public String getNombre(){
        return nombre;
    }
    public String getTipo(){
        return tipo;
    }
    public double getPrecio(){
        return precio;
    }
}
