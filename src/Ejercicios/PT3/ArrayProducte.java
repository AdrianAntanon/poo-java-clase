package Ejercicios.PT3;

import java.util.ArrayList;

/**
 @description: The creation of a class Producte, his development and it's starts up.
 @author: Adrian Antanyon
 @version: 01/06/2020
 */
class ArrayProducte {
    private ArrayList<Producte> lista_productos;

    public ArrayProducte(){
        lista_productos = new ArrayList<>();
    }

    public boolean agregarProducto(Producte productoNuevo){

        for(Producte agregarProductoNuevo: lista_productos){
            if (agregarProductoNuevo.getCodigo()==productoNuevo.getCodigo()){
                return false;
            }
        }

        lista_productos.add(productoNuevo);
        return true;
    }

    public String buscarProductoCodigo(int codigo){
        for (Producte busqueda: lista_productos){
            if (codigo == busqueda.getCodigo()){
                return "El producto " + busqueda.getNombre() + " es del tipo " + busqueda.getTipo()+
                        " y tiene un precio de "+ busqueda.getPrecio()+", además quedan " + busqueda.getStock() + " productos en stock";
            }
        }

        return "El código no existe";
    }

    public ArrayList<Producte> buscarProductoTipo(String tipo){
        ArrayList<Producte> buscarProductos = new ArrayList<>();

        for (Producte busqueda: lista_productos){
            if (busqueda.getTipo().equalsIgnoreCase(tipo)){
                buscarProductos.add(busqueda);
            }
        }

        return buscarProductos;
    }

    public String eliminarProducto(){
        boolean eliminado = false;

        for (Producte eliminacion: lista_productos){
            if (eliminacion.getStock()==0){
                lista_productos.remove(eliminacion);
                eliminado=true;
            }
        }

        if (!eliminado){
            return "El producto no existe";
        }else {
            return "Se ha eliminado todo el stock del producto";
        }
    }

    public String grandaria(){
        return "En el almacén existen " + lista_productos.size() + " productos.";
    }


    public void comprarProducto(int codigo){

        for (Producte compra: lista_productos){
            if (codigo==compra.getCodigo() && compra.getStock()>=1){
                compra.setStock(compra.getStock()-1);
            }
        }

    }

    public String informacionProductos(){
        String producto = "";

        for (Producte infoProducto: lista_productos){
            producto += "\nProducto: " + infoProducto.getNombre() + ", tipo: " + infoProducto.getTipo()+", código: "+infoProducto.getCodigo()+"" +
                    ", precio: " + infoProducto.getPrecio() + " y stock: " + infoProducto.getStock();
        }

        return producto;
    }








}
