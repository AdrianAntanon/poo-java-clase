package Ejercicios.PT3;

import java.util.ArrayList;
import java.util.Scanner;
/**
 @description: The creation of a class Producte, his development and it's starts up.
 @author: Adrian Antanyon
 @version: 01/06/2020
 */
public class FrmProducte extends ArrayProducte {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        FrmProducte programa = new FrmProducte();
        programa.inici();
    }


    public void inici(){
        boolean seguir = false;
        int seleccion=0;

        ArrayProducte nuevoProducto = new FrmProducte();

        do {
            seleccion=menu();

            switch (seleccion){
                case 1:
                    System.out.println("Introduce el código del producto, por favor:");
                    comprobacionInt();
                    int codigo = lector.nextInt();
                    nuevoProducto.comprarProducto(codigo);
                    break;
                case 2:
                    nuevoProducto = anyadirProducto(nuevoProducto);
                    break;
                case 3:
                    System.out.println("Introduce el código del producto que quieres ver, por favor");
                    comprobacionInt();
                    int cod = lector.nextInt();
                    nuevoProducto.buscarProductoCodigo(cod);
                    break;
                case 4:
                    nuevoProducto.eliminarProducto();
                    break;
                case 5:
                    aumentarPrecioOficina(nuevoProducto);
                    break;
                case 6:
                    System.out.println("Saliendo del programa, muchas gracias por visitar nuestra tienda");
                    seguir=true;
                    break;
                default:
                    System.out.println("Opción solo disponible para macho alfa espalda plateada");
            }


        }while (!seguir);
    }

    public int menu(){
        System.out.println("Escoge una opción, por favor: \n" +
                "1) Comprar producto\n" +
                "2) Añadir producto\n" +
                "3) Mostrar producto\n" +
                "4) Eliminar producto\n" +
                "5) Aumentar el precio productos tipo OFICINA\n" +
                "6) Salir");
        comprobacionInt();
        int eleccion = lector.nextInt();

        return eleccion;
    }

    public void comprobacionInt(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public void comprobacionDouble(){
        while (!lector.hasNextDouble()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public ArrayProducte anyadirProducto(ArrayProducte productoNuevo){
        System.out.println("Responde a las siguientes preguntas, por favor:");

        System.out.println("Código: ");
        comprobacionInt();
        int codigo = lector.nextInt();

        System.out.println("Precio: ");
        comprobacionDouble();
        double precio = lector.nextDouble();

        System.out.println("Tipo: ");
        String tipo = lector.next();

        System.out.println("Nombre: ");
        String nombre = lector.next();

        Producte nuevo = new Producte(codigo, nombre, tipo, precio);
        productoNuevo.agregarProducto(nuevo);

        return  productoNuevo;
    }

    public void aumentarPrecioOficina(ArrayProducte producto_oficina){
        ArrayList<Producte> lista_oficina = producto_oficina.buscarProductoTipo("Oficina");

        for (Producte aumento: lista_oficina){
            aumento.setPrecio(aumento.getPrecio()*1.10);
        }

    }
}
