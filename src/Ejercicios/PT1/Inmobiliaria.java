package Ejercicios.PT1;

import java.util.Scanner;

/**
 @description: Simulates a web portal of a real-estate.
 @author: Adrian Antanyon
 @version: 25/05/2020
 */
public class Inmobiliaria {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        Inmobiliaria programa = new Inmobiliaria();

        programa.inici();
    }

    public void inici(){
        boolean seguir=true;

        Piso vivienda = null;
        Local local = null;

        System.out.println("B I E N V E N I D O   A   L A   I N M O B I L I A R I A   C U A R E N T E N A\n");
        do {
            int opciones;
            menu();
            comprobacionInt();
            opciones = lector.nextInt();

            switch (opciones){
                case 1:
                    vivienda = introducirPiso(vivienda);
                    break;
                case 2:
                    local = introducirLocal(local);
                    break;
                case 3:
                    consultarInfoPiso(vivienda);
                    break;
                case 4:
                    consultarInfoLocal(local);
                    break;
                case 5:
                    vivienda = cambiarPrecioVivienda(vivienda);
                    break;
                case 6:
                    local = cambiarPrecioLocal(local);
                    break;
                default:
                    System.out.println("Opción no disponible, se requieren permisos de administrador para acceder.");
            }

            System.out.println("Quiere continuar consultando en C U A R E N T E N A ? \n" +
                    "Salir = NO, todo lo demás hará que continúe en C U A R E N T E N A...");
            String respuesta = lector.next().toUpperCase();
            lector.nextLine();

            if (respuesta.equals("NO")){
                seguir = false;
            }
        }while (seguir);

        System.out.println("Que pase un buen día, espero que haya disfrutado de su estancia en C U A R E N T E N A.");

    }

    public void menu(){
        System.out.println("Seleccione la opción que desea realizar, por favor:\n" +
                "1) Poner a la venta vivienda\n" +
                "2) Poner a la venta local\n" +
                "3) Consultar información sobre la vivienda \n" +
                "4) Consultar información sobre el local\n" +
                "5) Cambiar precio a vivienda\n" +
                "6) Cambiar precio a local");
    }

    public Piso introducirPiso(Piso vivienda_venta){
        System.out.println("Responda las siguientes preguntas, por favor:");

        System.out.println("Dirección vivienda:");
        lector.nextLine();
        String direccion_piso = lector.nextLine();


        System.out.println("Cantidad de m2:");
        comprobacionInt();
        int m2 = lector.nextInt();
        while (m2<1){
            System.out.println("Esos m2 son irreales, vuelva a introducirlos, por favor");
            comprobacionInt();
            m2=lector.nextInt();
        }

        System.out.println("Años desde su construcción:");
        comprobacionInt();
        int cantidad_anyos = lector.nextInt();
        while (cantidad_anyos<1){
            System.out.println("Esa cantidad de años es imposible, vuelva a introducirlos, por favor");
            comprobacionInt();
            cantidad_anyos=lector.nextInt();
        }

        System.out.println("Vivienda nueva o de segunda mano: (1-nueva 2-segunda mano)");
        boolean nuevo;
        comprobacionInt();
        int respuesta_nueva = lector.nextInt();
        while (respuesta_nueva<1){
            System.out.println("Respuesta incorrecta, vuelva a intentarlo, por favor");
            comprobacionInt();
            respuesta_nueva=lector.nextInt();
        }

        if (respuesta_nueva==1) nuevo = true;
        else nuevo = false;

        System.out.println("Tasación:");
        comprobacionInt();
        double precio_base = lector.nextInt();
        while (precio_base < 1){
            System.out.println("Tiene que ser un chiste, ojalá las viviendas no costasen nada o te pagasen por dartelas.\n" +
                    "Vuelve a introducirlo, por favor ");
            comprobacionInt();
            precio_base = lector.nextInt();
        }

        System.out.println("Planta: ");
        comprobacionInt();
        int planta = lector.nextInt();
        while (planta<0 || planta>50){
            System.out.println("Podría ser que vivas en un rascacielos o debajo del metro, pero no me lo creo.\n" +
                    "Vuelve a introducirlo, por favor");
            comprobacionInt();
            planta = lector.nextInt();
        }

        vivienda_venta = new Piso("piso", direccion_piso,m2,nuevo,precio_base,cantidad_anyos,planta);

        return vivienda_venta;

    }

    public Local introducirLocal(Local local_venta){
        System.out.println("Responda las siguientes preguntas, por favor:");

        System.out.println("Dirección local:");
        lector.nextLine();
        String direccion_piso = lector.nextLine();

        System.out.println("Cantidad de m2:");
        comprobacionInt();
        int m2 = lector.nextInt();
        while (m2<1){
            System.out.println("Esos m2 son irreales, vuelva a introducirlos, por favor");
            comprobacionInt();
            m2=lector.nextInt();
        }

        System.out.println("Años desde su construcción:");
        comprobacionInt();
        int cantidad_anyos = lector.nextInt();
        while (cantidad_anyos<1){
            System.out.println("Esa cantidad de años es imposible, vuelva a introducirlos, por favor");
            comprobacionInt();
            cantidad_anyos=lector.nextInt();
        }

        System.out.println("Local nuevo o de segunda mano: (1-nuevo 2-segunda mano)");
        boolean nuevo;
        comprobacionInt();
        int respuesta_nueva = lector.nextInt();
        while (respuesta_nueva<1){
            System.out.println("Respuesta incorrecta, vuelva a intentarlo, por favor");
            comprobacionInt();
            respuesta_nueva=lector.nextInt();
        }

        if (respuesta_nueva==1) nuevo = true;
        else nuevo = false;

        System.out.println("Tasación:");
        comprobacionInt();
        double precio_base = lector.nextInt();
        while (precio_base < 1){
            System.out.println("Tiene que ser un chiste, ojalá los locales no costasen nada o te pagasen por llevartelos.\n" +
                    "Vuelve a introducirlo, por favor ");
            comprobacionInt();
            precio_base = lector.nextInt();
        }

        System.out.println("Número ventanas: ");
        comprobacionInt();
        int num_ventanas = lector.nextInt();
        while (num_ventanas<0 || num_ventanas>50){
            System.out.println("Imposible tener ventanas negativias o que el local sea todo cristales, no me lo creo.\n" +
                    "Vuelve a introducirlo, por favor");
            comprobacionInt();
            num_ventanas = lector.nextInt();
        }

        local_venta = new Local("local", direccion_piso,m2,nuevo,precio_base,cantidad_anyos,num_ventanas);

        return local_venta;
    }
    public Piso cambiarPrecioVivienda(Piso vivienda){
        if (vivienda == null){
            System.out.println("Todavía no has introducido ninguna vivienda, no es posible realizar la operación");
        }else {
            System.out.println("Introduzca el precio nuevo, por favor\n" +
                    "El precio actual es de " + vivienda.getPrecio_base()+"€");
            comprobacionInt();
            int precio_nuevo = lector.nextInt();
            while (precio_nuevo < 1){
                System.out.println("Precio imposible.\n" +
                        "Vuelve a introducirlo, por favor ");
                comprobacionInt();
                precio_nuevo = lector.nextInt();
            }

            vivienda.setPrecio_base(precio_nuevo);

            System.out.println("Precio actualizado: "+vivienda.getPrecio_base()+"€");
        }

        return vivienda;

    }

    public Local cambiarPrecioLocal(Local local){

        if (local == null){
            System.out.println("Todavía no has introducido ningún local, no es posible realizar la operación");
        }else {
            System.out.println("Introduzca el precio nuevo, por favor\n" +
                    "El precio actual es de " + local.getPrecio_base()+"€");
            comprobacionInt();
            int precio_nuevo = lector.nextInt();
            while (precio_nuevo < 1){
                System.out.println("Precio imposible.\n" +
                        "Vuelve a introducirlo, por favor ");
                comprobacionInt();
                precio_nuevo = lector.nextInt();
            }

            local.setPrecio_base(precio_nuevo);

            System.out.println("Precio actualizado: "+local.getPrecio_base()+"€");
        }

        return local;

    }
    public void consultarInfoPiso(Piso vivienda){

        if (vivienda == null){
            System.out.println("Todavía no has introducido ninguna vivienda, no es posible realizar la operación");
        }else {
            System.out.println(vivienda.getInfoPiso());
        }

    }

    public void consultarInfoLocal(Local local){
        if (local == null){
            System.out.println("Todavía no has introducido ninguna local, no es posible realizar la operación");
        }else {
            System.out.println(local.getInfoLocal());
        }
    }

    public void comprobacionInt(){
        while (!lector.hasNextInt()){
            System.out.println("No es un valor permitido, vuelva a introducirlo, por favor.");
            lector.next();
        }
    }
}

class Inmueble{
    private String tipo_inmueble, direccion_inmueble;
    private int m2, cantidad_anyos;
    private boolean nuevo;
    private double precio_base;

    public Inmueble(String tipo_inmueble, String direccion_inmueble, int m2, boolean nuevo, double precio_base, int cantidad_anyos){

        this.tipo_inmueble = tipo_inmueble;
        this.direccion_inmueble = direccion_inmueble;
        this.m2 = m2;
        this.nuevo=nuevo;
        this.cantidad_anyos=cantidad_anyos;

        if (cantidad_anyos <= 15){
            this.precio_base = precio_base*0.99;
        }else {
            this.precio_base = precio_base*0.98;
        }
    }

    public String getInfoInmueble(){
        if (nuevo){
            return "Es un " + tipo_inmueble + " nuevo valorado en "+ precio_base +"€ de "+ cantidad_anyos +" años situado en " + direccion_inmueble + " de " + m2 + " metros cuadrados";
        }else {
            return "Es un " + tipo_inmueble + " de segunda mano valorado en "+ precio_base +"€ de "+ cantidad_anyos +" años situado en " + direccion_inmueble + " de " + m2 + " metros cuadrados";
        }
    }


    public void setPrecio_base(double precio_nuevo){
        precio_base = precio_nuevo;

        if (cantidad_anyos <= 15){
            precio_base = precio_base*0.99;
        }else {
            precio_base = precio_base*0.98;
        }
    }

    public double getPrecio_base(){
        return precio_base;
    }

}

class Piso extends Inmueble{

    private int planta;

    public Piso(String tipo_inmueble, String direccion_inmueble, int m2, boolean nuevo, double precio_base, int cantidad_anyos, int planta) {
        super(tipo_inmueble, direccion_inmueble, m2, nuevo, precio_base, cantidad_anyos);
        this.planta=planta;
    }

    public String getInfoPiso(){
        return getInfoInmueble() + " en la planta " + planta;
    }
}

class Local extends Inmueble{

    private int num_ventanas;

    public Local(String tipo_inmueble, String direccion_inmueble, int m2, boolean nuevo, double precio_base, int cantidad_anyos,int num_ventanas) {
        super(tipo_inmueble, direccion_inmueble, m2, nuevo, precio_base, cantidad_anyos);
        this.num_ventanas=num_ventanas;
    }

    public String getInfoLocal(){
        if (num_ventanas==1){
            return getInfoInmueble() + " con " + num_ventanas + " ventanal";
        }else if (num_ventanas==0){
            return getInfoInmueble() + " con ningún escaparate o ventanas";
        }else {
            return getInfoInmueble() + " con " + num_ventanas + " ventanales";
        }

    }
}