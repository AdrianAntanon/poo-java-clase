package Ejercicios.PT1;
/**
 @description: Simulates the behavior of institute with students, professors and administratives.
 @author: Adrian Antanyon
 @version: 23/05/2020
 */
public class Instituto {
    public static void main(String[] args) {

        Alumno adrian = new Alumno("Adrián", "Antañón", "39444363F","Desarrollo de Aplicaciones Web", 1);

        System.out.println(adrian.getInfoAlumno());

        adrian.setCiclo("Administración y Finanzas");
        adrian.setAnyo_ciclo(2);

        System.out.println(adrian.getInfoAlumno());
        System.out.println();

        Profesor javier = new Profesor("Javier","Gongora","66644432H","Desarrollo de Aplicaciones Web",1850.83,35.5);

        System.out.println(javier.getInfoProfesor());
        javier.setHoras_trabajadas(30);
        System.out.println(javier.getInfoProfesor());
        System.out.println();

        Administrativo isaac = new Administrativo("Isaac","Pancracio","98463721A",1700);

        System.out.println(isaac.getInfoAdministrativo());
        isaac.setSubeSueldo(150);
        isaac.setBajaSueldo(50);
        System.out.println(isaac.getInfoAdministrativo());

    }
}

class Persona{

    private String nombre, apellido, nif;

    public Persona(String nombre, String apellido, String nif){
        this.nombre = nombre;
        this.apellido = apellido;
        this.nif = nif;
    }

    public String getInformacion(){
        return "es " + nombre + " " + apellido + " y tiene el NIF " + nif;
    }
}

class Alumno extends Persona{

    private String nombre_ciclo;
    private int anyo_ciclo;

    public Alumno(String nombre, String apellido, String nif, String nombre_ciclo, int anyo_ciclo) {
        super(nombre, apellido, nif);
        this.nombre_ciclo = nombre_ciclo;
        this.anyo_ciclo = anyo_ciclo;
    }

    public void setCiclo(String ciclo_nuevo){
        nombre_ciclo = ciclo_nuevo;
    }

    public String getInfoAlumno(){
        return "El nombre del alumno " + getInformacion() + ".\n" +
                "Estudia " + anyo_ciclo + "º de " + nombre_ciclo;
    }

    public String getNombre_y_anyo_ciclo(){
        return "El alumno cursa el año " + anyo_ciclo + " de " + nombre_ciclo;
    }
    public void setAnyo_ciclo(int anyo_ciclo){
        this.anyo_ciclo=anyo_ciclo;
    }

}

class Profesor extends Persona{

    private String nombre_ciclo;
    private double sueldo, horas_trabajadas;


    public Profesor(String nombre, String apellido, String nif, String nombre_ciclo, double sueldo, double horas_trabajadas) {
        super(nombre, apellido, nif);
        this.nombre_ciclo=nombre_ciclo;
        this.sueldo=sueldo;
        this.horas_trabajadas=horas_trabajadas;
    }

    public String getInfoProfesor(){
        return "El nombre del profesor " + getInformacion() + ".\n" +
                "Cobra " + sueldo + "€ y trabaja " + horas_trabajadas + " horas semanales";
    }

    public void setHoras_trabajadas(double horas_trabajadas){
        if(horas_trabajadas >= 20){
            this.horas_trabajadas = horas_trabajadas;
        }else {
            System.out.println("No permitido el cambio de horario.");
        }
    }
}

class Administrativo extends Persona{

    private double sueldo;

    public Administrativo(String nombre, String apellido, String nif, double sueldo) {
        super(nombre, apellido, nif);
        this.sueldo=sueldo;
    }

    public String getInfoAdministrativo(){
        return "El nombre del admnistrativo " + getInformacion() + " y tiene un sueldo de " + sueldo+"€";
    }

    public void setSubeSueldo(double aumento){
        sueldo += aumento;
    }

    public void setBajaSueldo(double decremento){
        sueldo -= decremento;
    }
}
