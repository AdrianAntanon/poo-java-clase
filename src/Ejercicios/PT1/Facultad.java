package Ejercicios.PT1;
/**
 @description: Differents types of class from Collegue
 @author: Adrian Antanyon
 @version: 23/05/2020
 */
public class Facultad {
    public static void main(String[] args) {

        Alumne adrian = new Alumne("Adri","Anta","39444363F",false,"DAWe");

    }
}

class Individuo{
    private String nombre, apellido, nif;
    private boolean estado_civil;

    public Individuo(String nombre, String apellido, String nif, boolean estado_civil){
        this.nombre = nombre;
        this.apellido=apellido;
        this.nif=nif;
        this.estado_civil=estado_civil;
    }
}

class Alumne extends Individuo{

    private String curso;

    public Alumne(String nombre, String apellido, String nif, boolean estado_civil, String curso) {
        super(nombre, apellido, nif, estado_civil);
        this.curso=curso;
    }
}

class Profe extends Individuo{

    private int anyo_incorporacion, num_despacho;
    private String departamento;

    public Profe(String nombre, String apellido, String nif, boolean estado_civil, int anyo_incorporacion, int num_despacho, String departamento) {
        super(nombre, apellido, nif, estado_civil);
        this.anyo_incorporacion=anyo_incorporacion;
        this.num_despacho=num_despacho;
        this.departamento=departamento;
    }
}

class PersonaServicio extends Individuo{

    private int anyo_incorporacion, num_despacho;
    private String seccion;

    public PersonaServicio(String nombre, String apellido, String nif, boolean estado_civil, int anyo_incorporacion, int num_despacho, String seccion) {
        super(nombre, apellido, nif, estado_civil);
        this.anyo_incorporacion=anyo_incorporacion;
        this.num_despacho=num_despacho;
        this.seccion=seccion;
    }
}
