package Ejercicios.PT2;
/**
 @description: A simulation of a supermarket and his employees
 @author: Adrian Antanyon
 @version: 28/05/2020
 */
public class SuperMerK2 {
    public static void main(String[] args) {
        Mostrador adri = new Mostrador("Adri","Barcelona","Prosperitat",1000);

        System.out.println("Ventas totales del 28/05/2020 "+adri.getVentas()+"€");
        System.out.println(adri.getNombre()+" tiene un sueldo diario de "+adri.getSueldoDiario()+"€");

        Limpieza clara = new Limpieza("Clara","Badajoz","Canyelles");

        System.out.println(clara.getNombre() +" tiene un sueldo diario de " +clara.getSueldoDiario() + "€");

        Cajero andrea = new Cajero("Andrea","Barcelona","El Turó de la Peira",9);

        System.out.println(andrea.getNombre() + " tiene un sueldo diario de " + andrea.getSueldoDiario()+"€");
    }
}

abstract class Empleado{
    private String nombre, ciudad_origen, lugar;

    public Empleado(String nombre, String ciudad_origen, String lugar){
        this.nombre = nombre;
        this.ciudad_origen = ciudad_origen;
        this.lugar = lugar;
    }

    public String getNombre(){
        return nombre;
    }

    public String getLugar(){
        return lugar;
    }

    public String getCiudad_origen(){
        return ciudad_origen;
    }

    public abstract double getSueldoDiario();
}

class Cajero extends Empleado{
    private int horas_trabajadas;
    private double sueldo_diario = 15;
    public Cajero(String nombre, String ciudad_origen, String lugar, int horas_trabajadas) {
        super(nombre, ciudad_origen, lugar);
        this.horas_trabajadas = horas_trabajadas;

        sueldo_diario *= horas_trabajadas;
    }

    public int getHoras_trabajadas(){
        return horas_trabajadas;
    }

    @Override
    public double getSueldoDiario() {
        return sueldo_diario;
    }
}

class Limpieza extends Empleado{
    private double sueldo_diario = 35;
    public Limpieza(String nombre, String ciudad_origen, String lugar) {
        super(nombre, ciudad_origen, lugar);
    }

    @Override
    public double getSueldoDiario() {
        return sueldo_diario;
    }
}

class Mostrador extends Empleado{
    private double ventas;
    private double sueldo_diario = 50;
    public Mostrador(String nombre, String ciudad_origen, String lugar, double ventas) {
        super(nombre, ciudad_origen, lugar);
        this.ventas = ventas;

        sueldo_diario += (ventas*0.15);
    }

    public double getVentas(){
        return ventas;
    }

    @Override
    public double getSueldoDiario() {
        return sueldo_diario;
    }
}
