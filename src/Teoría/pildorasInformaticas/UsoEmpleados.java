package Teoría.pildorasInformaticas;

import java.util.GregorianCalendar;

public class UsoEmpleados {
    public static void main(String[] args) {
        Empleados Adri = new Empleados("Adri", 1800, 2019, 12, 25);

        Jefes Alex = new Jefes("Alex", 2100, 2019, 11, 01);

        Alex.setIncentivo(500);

        System.out.println(Alex.getSueldo());
    }
}

class Empleados{
    private final String nombre;
    private int Id;
    private static int IdSiguiente = 1;
    private double sueldo;
    GregorianCalendar calendrario;

    public Empleados(String nombre, double sueldo, int agno, int mes, int dia){
        this.nombre = nombre;
        this.sueldo = sueldo;

        calendrario = new GregorianCalendar(agno,mes,dia);

        Id = IdSiguiente;
        IdSiguiente++;

    }

    public String getDatosEmpleado(){

        return "El empleado " + nombre + " y tiene el nº de identificación " + Id;
    }

    public double getSueldo(){
        return sueldo;
    }

    public GregorianCalendar getFechaAlta(){
        return calendrario;
    }

    public void setSubeSueldo(double porcentaje){
        double aumento = sueldo * porcentaje/100;
        sueldo += aumento;
    }
}

class Jefes extends Empleados {
    private double incentivo;
    public Jefes(String nombre, double sueldo, int agno, int mes, int dia) {
        super(nombre, sueldo, agno, mes, dia);
    }

    public void setIncentivo(double b){
        incentivo = b;

    }

    public double getSueldo(){
        double sueldoJefe = super.getSueldo();

        return sueldoJefe+incentivo;

    }

}