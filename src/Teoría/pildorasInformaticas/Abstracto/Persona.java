package Teoría.pildorasInformaticas.Abstracto;

import java.util.Date;

public abstract class Persona {
    private String nom;
    public Persona(String nombre){
        nom = nombre;
    }

    public String getNom(){
        return nom;
    }

    public abstract String getDescripcion();
}


class Empleados extends Persona{

    private double sueldo;
    private Date fechaAlta;

    public Empleados(String nombre, double sueldo, Date fechaAlta) {
        super(nombre);
        this.sueldo=sueldo;
        this.fechaAlta = fechaAlta;
    }

    @Override
    public String getDescripcion() {
        return "El empleado " + this.getNom() + " tiene un sueldo de " + sueldo +"€ y entró a trabajar el " + fechaAlta;
    }


}