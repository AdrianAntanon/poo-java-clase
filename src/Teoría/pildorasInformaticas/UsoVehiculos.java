package Teoría.pildorasInformaticas;

/*Ejemplo de modularización*/

public class UsoVehiculos { // Clase principal
    public static void main(String[] args) {

        Vehiculos cochePrueba = new Vehiculos(4,3,2,1); // Llamada al constructor de Vehiculos.java

        System.out.println("El color del coche es: "+cochePrueba.getColor()); // Nos devuelve el color del coche POR DEFECTO.

        cochePrueba.setColor("Verde"); // Asignamos el color que queramos

        System.out.println("El color del coche es: "+cochePrueba.getColor());//Nos devuelve el color que hemos asignado previamente.

        System.out.println(cochePrueba.getInfoVehiculo());

        Vehiculos motoPrueba = new Vehiculos(2, 2, 1, 1);

        System.out.println(motoPrueba.getInfoVehiculo());

        System.out.println(cochePrueba.getExtra());
        cochePrueba.setExtras(true);
        System.out.println(cochePrueba.getExtra());
    }

}
