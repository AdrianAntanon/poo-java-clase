package Teoría.pildorasInformaticas;

public class Vehiculos { // Clase

    // Declaraciones de propiedades que tendrá la clase, hacemos que SOLO sea accesible desde esta clase añadiendo PRIVATE
    private int ruedas, largo, ancho, peso;
    private String color;
    private boolean climatizador, tapiceriaCuero, gps;


    public Vehiculos( int ruedas, int largo, int ancho, int peso){ // Constructor de la clase, siempre tiene que tener el mismo nombre que la clase
        this.ruedas = ruedas;
        this.largo = largo;
        this.ancho = ancho;
        this.peso = peso;
        color = "Sin color";

        // Por defecto todos los coches de clase Vehículos tendrán estas características, es como si creasemos un molde sobre el que trabajaremos
    }

    public Vehiculos(int ruedas, int peso){ // sobrecarga de contructores

        this.ruedas = ruedas;
        this.peso = peso;
        color = "sin color";
        largo = 4;
        ancho = 2;
    }

    public String getInfoVehiculo(){ // Mensaje que le llegará con los datos del vehículo
        if (peso >1){
            return "Tu vehículo dispone de "+ ruedas + " ruedas, mide "+largo+" metros de largo por "+ancho+" de ancho, por último pesa "+peso+" toneladas";
        }else{
            return "Tu vehículo dispone de "+ ruedas + " ruedas, mide "+largo+" metros de largo por "+ancho+" de ancho, por último pesa "+peso+" tonelada";

        }

    }

    public void setColor(String color){ // SETTER --> por convención, es RECOMENDABLE usar siempre la palabra SET para asignar un valor a una propiedad, así que en vez de ser asignarColor() es setColor()

        this.color = color; //El this sirve para que el compilador DIFERENCIE entre la variable global y la que la local (que es la que pasamos por parámetro), SOLO EN CASO DE QUE TENGAN EL MISMO NOMBRE, THIS hace referencia a la variable global!!!.
    }

    public void setExtras(boolean climatizador){
        this.climatizador = climatizador;
    }
    //Ejemplo de sobrecarga de métodos
    public void setExtras(boolean climatizador, boolean tapiceriaCuero, boolean gps){
        this.climatizador = climatizador;
        this.tapiceriaCuero = tapiceriaCuero;
        this.gps = gps;
    }

    public String getExtra(){

        if(climatizador && gps==false && tapiceriaCuero==false){
            return "El vehículo dispone del pack 1 de extras";
        }else if(climatizador && gps && tapiceriaCuero){
            return "El vehículo dispone del pack 2 de extras";
        }else{
            return "El vehículo no dispone de extras";
        }
    }

    public String getColor(){ // GETTER --> por convención, al igual que arriba ponemos set aquí pasa lo mismo con GET, se usa para devolver un valor

        return color;

    }
}
